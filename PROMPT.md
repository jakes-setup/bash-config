# Prompt

1. [ ] ~~If in chroot: "(chroot) "~~
1. [x] BYellow + "[" + Time + "] "
1. [ ] If last command took more than 5 seconds: BYellow + "(took: 2d3h4m1s) "
1. [ ] ~~If UID == 0: Red + Hostname + " " + CurrentWorkDir + " -> " + ResetColor ; return~~
1. [x] If User not Jake: BGreen + Username + BWhite + "@"
1. [x] BPurple + Hostname
1. [x] BWhite + ":"
1. [x] BIBlue
1. [x] If in Git Dir:
	1. IYellow + GitBaseName
	1. If in GitSubDir: "/" + GitSubDir
1. [x] If not in Git Dir: CurrentWorkDir
1. [x] " "
1. [x] If in Git Dir:
	1. ...
1. [ ] If running background jobs: Yellow  + "[bg:" + BackgroundJobs + "] "
1. [ ] If stopped background jobs: Yellow  + "[stp:" + StoppedBackgroundJobs + "] "
1. [ ] If z Support: ... -- https://github.com/rupa/z
1. [x] If Exit == 0: White
1. [x] If Exit != 0: Red
1. [x] "$ " + ResetColor

