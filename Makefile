.PHONY: default
default: run install

.PHONY: run
run: build
	@echo ""
	time ./bashprompt
	@echo ""

.PHONY: install
install: clean build
	go install

.PHONY: build
build:
	go get
	go mod tidy
	go list
	go build

.PHONY: clean
clean:
	go clean

