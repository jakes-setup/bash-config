#!/bin/bash

set -e
#set -x

echo "Updating bash config..."

pushd "$(dirname "$0")" >/dev/null

git submodule update --init --recursive --remote
#git submodule init
#git submodule update
#git pull --recurse-submodules --verbose --force # segfaulted
git pull
git submodule update --init --recursive --remote

# Compile and Install bash-prompt
make install


# Install fzf
./fzf/install --all --no-update-rc --no-zsh --no-fish --completion

# Install fastfetch
FFVERSION=2.21.1
if [ ! -x "./fastfetch" ] || [ "$(./fastfetch --version)" != "fastfetch ${FFVERSION} (x86_64)" ]; then
	# fastfetch-1.11.0-Linux/usr/bin/flashfetch
	#wget "https://github.com/LinusDierheimer/fastfetch/releases/download/${FFVERSION}/fastfetch-${FFVERSION}-Linux.zip" -O fastfetch.zip
	wget "https://github.com/LinusDierheimer/fastfetch/releases/download/${FFVERSION}/fastfetch-linux-amd64.zip" -O fastfetch.zip
	unzip -o fastfetch.zip
	rm -f fastfetch.zip
	rm -f fastfetch
	#ln -s "fastfetch-${FFVERSION}-Linux/usr/bin/fastfetch" fastfetch
	ln -s "fastfetch-linux-amd64/usr/bin/fastfetch" fastfetch
fi

function install_symlink() {
	SOURCE="$1"
	TARGET="$2"

	if [ ! -L "${TARGET}" ] && [ -e "${TARGET}" ]; then
		echo "${TARGET} is not a symlink. Creating backup..."
		mv -vf "${TARGET}" "${TARGET}-backup-$(date +%Y%m%d-%H%M%S)"
	fi

	rm -f "${TARGET}"

	SOURCE="$(realpath -e -L "${SOURCE}")"
	TARGET="$(realpath  -L "${TARGET}")"

	ln -v -s -f  "${SOURCE}" "${TARGET}"
}

install_symlink "bashrc.sh" "${HOME}/.bashrc"
install_symlink "bash_aliases.sh" "${HOME}/.bash_aliases"
install_symlink "bash_logout.sh" "${HOME}/.bash_logout"
install_symlink "profile.sh" "${HOME}/.profile"

if [ -e "${HOME}/.bash_profile" ]; then
	echo -ne "\n\n"
	echo "======= WARNING ======="
	echo "${HOME}/.bash_profile exists. This may cause problems."
	echo "======= /WARNING ======="
	echo -ne "\n\n"
fi

if [ -e "${HOME}/.bash_login" ]; then
	echo -ne "\n\n"
	echo "======= WARNING ======="
	echo "${HOME}/.bash_login exists. This may cause problems."
	echo "======= /WARNING ======="
	echo -ne "\n\n"
fi

popd >/dev/null

echo "Updated bash config."
echo "Please run 'source ~/.profile'"
