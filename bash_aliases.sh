

alias ls='ls -lahF --color=auto'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias cd..='cd ..'
alias cls='clear'
alias dir='ls'
alias car='cat'
alias grep='grep --color=auto'

alias cp='cp -vr'
alias rm='rm -v'
alias mv='mv -v'

alias tmux='tmux -u'

# TYPOS
alias sudp='sudo '
alias babi='nano '
alias igit='git'
alias itgit='git'
alias ggit='git'

if [ "$HOSTNAME" = "RechenApparat" ]; then
	# Gentoo
	alias update='sudo eix-sync ; sudo emerge --update --deep --newuse @world'
elif [ "$HOSTNAME" = "RechenMonster" ]; then
	# Manjaro
	alias update='yay -Syyu'
else
	# Debian / Ubuntu
	alias update='sudo apt-get update; sudo apt-get upgrade -y; sudo apt-get dist-upgrade -y'
fi

alias nano='echo "halts maul du penner und nutz nvim"'
alias v='nvim'
alias vim='nvim'

alias sl="sl | lolcat -tf"

alias g='git'
alias gits='git status'

alias netstat-tulpn='sudo netstat -tulpn'

alias yt='youtube-dl --add-metadata -i -c -o "%(title)s.%(ext)s" --write-thumbnail --restrict-filenames --geo-bypass '
alias yta='yt --extract-audio --audio-format mp3 --audio-quality 0 '

#alias yt='youtube-dl --add-metadata -i -c -o "%(title)s.%(ext)s" --write-thumbnail --restrict-filenames --geo-bypass --embed-subs --embed-thumbnail '
#alias yta='yt --extract-audio --audio-format mp3 --audio-quality 0 '
#alias syt='sudo youtube-dl --add-metadata -i -c -o "%(title)s.%(ext)s" --write-thumbnail --restrict-filenames --geo-bypass --embed-subs --embed-thumbnail '
#alias syta='syt --extract-audio --audio-format mp3 --audio-quality 0 '
##alias ytap="cat download.txt | xargs -n1 youtube-dl -o '%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' --extract-audio --audio-format mp3 --audio-quality 0  --add-metadata -i -c --write-thumbnail --restrict-filenames --geo-bypass  --embed-subs --embed-thumbnail  "
#alias ytap="youtube-dl -o '%(uploader)s/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' --extract-audio --audio-format mp3 --audio-quality 0 --yes-playlist --add-metadata -i -c --write-thumbnail --restrict-filenames --geo-bypass  --embed-subs --embed-thumbnail "


#alias ghci='stack ghci'
#alias ghc='stack ghc'
#alias runghci='stack runghci'
#alias runghc='stack runghc'
#alias cabal='echo "halts maul du penner und nutz stack"'
#
#alias gentooprefixstart='~/festplatte/current/stuff/j.vondoemming/gentooprefix/prefix/startprefix'


# Better make
alias make="makehelper.sh"

# Logout session when desktop ends.
alias startx='exec startx'
