package main

import (
	"encoding/json"
	"fmt"
	"math/rand/v2"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Message struct {
	name  string
	msg   string
	iserr bool
}

// PS1Part represents an individual part of the PS1 string
type PS1Part struct {
	value []string
	vars  map[string]string
	msgs  []string
	cmds  []string
}

// PS1PartBuilder is a function that returns a PS1Part instance and an error
type PS1PartBuilder func() (*PS1Part, error)

// NewPS1Part returns a new PS1Part instance
func NewPS1Part(a ...string) *PS1Part {
	return &PS1Part{value: a}
}
func NewPS1PartWithExtra(vars map[string]string, msgs []string, cmds []string, a ...string) *PS1Part {
	return &PS1Part{
		value: a,
		vars:  vars,
		msgs:  msgs,
		cmds:  cmds,
	}
}

func buildPythonVenvPart() (*PS1Part, error) {
	venv_path := os.Getenv("VIRTUAL_ENV")
	if venv_path == "" {
		return NewPS1Part(), nil
	}

	// determine whether venv still exists
	venv_info, err := os.Stat(venv_path)
	does_venv_exist := (err == nil && venv_info.IsDir())

	// determine if current dir is in the project where the venv is located
	project_path := filepath.Dir(venv_path)
	project_path, err = AbsEvalSymlinks(project_path)
	if err != nil {
		return nil, err
	}
	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	cwd, err = AbsEvalSymlinks(cwd)
	if err != nil {
		return nil, err
	}
	relative_path, err := filepath.Rel(project_path, cwd)
	if err != nil {
		return nil, err
	}
	is_cwd_in_same_project_as_venv := !(strings.HasPrefix(relative_path, "../") || relative_path == "..")

	// use the color in the prompt to indicate state
	var venv_color string
	if !does_venv_exist {
		venv_color = FGRedBold
	} else if !is_cwd_in_same_project_as_venv {
		venv_color = FGCyanBold
	} else {
		venv_color = FGWhiteBold
	}

	return NewPS1Part(venv_color, "(venv) "), nil
}

func buildTimewarriorPart() (*PS1Part, error) {
	lasttimewcheckts_str := os.Getenv("LASTTIMEWCHECKTS")
	if lasttimewcheckts_str == "" {
		lasttimewcheckts_str = "0"
	}
	lasttimewcheckts_int, err := strconv.ParseInt(lasttimewcheckts_str, 10, 64)
	if err != nil {
		return nil, err
	}
	lasttimewcheckts := time.Unix(lasttimewcheckts_int, 0)
	diff := time.Now().Sub(lasttimewcheckts)
	diff = diff.Round(time.Second) // remove milliseconds

	if diff.Seconds() < 60*5 {
		return NewPS1Part(), nil
	}

	timewCmd := exec.Command("timew")
	timewOutputBytes, _ := timewCmd.CombinedOutput() // timew also returns 1 if there is no active time tracking, thus we ignore the error
	// timewOutputBytes, err := timewCmd.CombinedOutput()
	//if err != nil {
	//	return NewPS1Part(), nil
	//}

	timewOutput := string(timewOutputBytes[:])
	if strings.Contains(timewOutput, "There is no active time tracking.") {
		cwd, err := os.Getwd()
		if err != nil {
			return nil, err
		}
		lasttimewcwd := os.Getenv("LASTTIMEWCWD")
		if lasttimewcwd == cwd {
			// we are still in the same directory
			return NewPS1Part(), nil
		}

		// try searching for a timewarrior.json file
		timewarriorReminderPath, err := FindFile([]string{"timewarrior.json", ".timewarrior.json"}, cwd)
		if err != nil {
			// no such file found
			return NewPS1Part(), nil
		}

		// open the file
		timewarriorReminderFile, err := os.Open(timewarriorReminderPath)
		if err != nil {
			return nil, fmt.Errorf("Failed to open file \"%s\": %s", timewarriorReminderPath, err)
		}
		defer timewarriorReminderFile.Close()

		// parse the file
		type TimewarriorReminder struct {
			Task   string `json:"task"`
			Cancel bool   `json:"cancel"`
		}
		var timewarriorReminder TimewarriorReminder
		err = json.NewDecoder(timewarriorReminderFile).Decode(&timewarriorReminder)
		if err != nil {
			return nil, fmt.Errorf("Failed to decode file \"%s\": %s", timewarriorReminderPath, err)
		}
		if timewarriorReminder.Cancel {
			// don't continue searching
			return NewPS1Part(), nil
		}
		if timewarriorReminder.Task == "" {
			return nil, fmt.Errorf("Missing 'task' value in \"%s\".", timewarriorReminderPath)
		}

		// print reminder
		vars := map[string]string{
			"LASTTIMEWCWD": cwd,
		}
		msgs := []string{fmt.Sprintf("Track your time!\n    timew start %s", timewarriorReminder.Task)}
		cmds := []string{}
		return NewPS1PartWithExtra(vars, msgs, cmds), nil
	} else if strings.HasPrefix(timewOutput, "Tracking") {
		// already tracking
		vars := map[string]string{
			"LASTTIMEWCHECKTS": strconv.FormatInt(time.Now().Unix(), 10),
		}
		msgs := []string{timewOutput}
		cmds := []string{}
		return NewPS1PartWithExtra(vars, msgs, cmds), nil
	} else {
		// timew not installed or weird error we don't understand
		return NewPS1Part(), nil
	}
}

func buildTimePart() (*PS1Part, error) {
	prevpromptts_str := os.Getenv("PREVPROMPTTS")
	if prevpromptts_str == "" {
		prevpromptts_str = "0"
	}
	prevpromptts_int, err := strconv.ParseInt(prevpromptts_str, 10, 64)
	if err != nil {
		return nil, err
	}
	prevpromptts := time.Unix(prevpromptts_int, 0)
	diff := time.Now().Sub(prevpromptts)
	diff = diff.Round(time.Second) // remove milliseconds

	if diff.Seconds() > 5 {
		return NewPS1Part(FGYellowBold, "[\\t]", "(", diff.String(), ") "), nil
	} else {
		return NewPS1Part(FGYellowBold, "[\\t] "), nil
	}
}

// buildUsernamePart returns a PS1Part instance for the username
func buildUsernamePart() (*PS1Part, error) {
	var mice = [...]string{
		"🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁", "🐁",
		"🐭", "🐭", "🐭", "🐭", "🐭", "🐭", "🐭", "🐭", "🐭",
		"🐀", "🐀", "🐀",
		"🧀",
	}
	username := os.Getenv("USER")
	switch username {
	case "":
		return nil, fmt.Errorf("username not set")
	case "jake", "j.vondoemming", "deck":
		return NewPS1Part(), nil
	case "maus":
		return NewPS1Part(FGBrightWhite, mice[rand.IntN(len(mice))], "@"), nil
	case "root":
		return NewPS1Part(FGBrightRedBold, username, FGBrightWhite, "@"), nil
	default:
		return NewPS1Part(FGGreenBold, username, FGBrightWhite, "@"), nil
	}
}

// buildHostnamePart returns a PS1Part instance for the hostname
func buildHostnamePart() (*PS1Part, error) {
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	return NewPS1Part(FGMagentaBold, hostname, FGBrightWhiteBold, ":"), nil
}

// buildCwdPart returns a PS1Part instance for the current working directory
func buildCwdPart() (*PS1Part, error) {
	//cwd, err := os.Getwd()
	//if err != nil {
	//	return nil, err
	//}
	return NewPS1Part(FGBrightBlueBold, "\\w"), nil
}

func buildGitPart() (*PS1Part, error) {
	cwd, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	cwd, err = AbsEvalSymlinks(cwd)
	if err != nil {
		return nil, err
	}
	repos, err := getGitRootDirs(cwd)
	if err != nil {
		return nil, err
	}

	if len(repos) == 0 {
		// default case if not in a git repository
		return buildCwdPart()
	}

	res := make([]string, 0) // result

	// ==== warn me if the current repo is in /tmp
	if strings.HasPrefix(repos[0], "/tmp/") {
		res = append(res, FGRedBold)
		res = append(res, "/tmp/")
	}
	// ==== warn me if the current repo is in SCRATCH
	if strings.Contains(repos[0], "/SCRATCH/") {
		res = append(res, FGRedBold)
		res = append(res, "SCRATCH/")
	}

	// ==== repository names
	for i, rootDir := range repos {
		name := filepath.Base(rootDir)
		if i > 0 {
			res = append(res, FGBrightWhiteBold)
			res = append(res, "/")
		}
		res = append(res, FGBrightYellow)
		res = append(res, name)
	}

	rootDir := repos[len(repos)-1] // current repo

	// ==== relative dir in repo
	rootDir, err = AbsEvalSymlinks(rootDir)
	if err != nil {
		return nil, err
	}
	relPath, err := filepath.Rel(rootDir, cwd)
	if err != nil {
		return nil, err
	}
	if relPath != "." {
		res = append(res, FGBrightBlueBold)
		res = append(res, "/")
		res = append(res, relPath)
	}
	res = append(res, " ")

	// ==== status

	status, err := getGitStatus(rootDir)
	if err != nil {
		return nil, err
	}

	numOrd := len(status.ChangedOrd)
	numCopy := len(status.ChangedCopy)
	numUnmerged := len(status.Unmerged)
	numUntracked := len(status.Untracked)
	//numIgnored := len(status.Ignored)

	// set branch color
	if numOrd+numCopy+numUnmerged+numUntracked > 0 {
		res = append(res, FGRed)
	} else {
		res = append(res, FGGreen)
	}
	res = append(res, "[")
	if status.Branch.Head != "(detached)" {
		if status.Branch.Head == "main" {
			res = append(res, "m") // replace "main" with "m"
		} else {
			res = append(res, status.Branch.Head)
		}
	} else {
		// detached
		res = append(res, status.Branch.OID[:8])
	}
	//tags, err := get_tags_by_hash(rootDir, head.Hash())
	//for _, tag := range tags {
	//	res = append(res, " ")
	//	res = append(res, tag.Name().Short())
	//}
	res = append(res, "]")

	// ahead and behind
	ahead := status.Branch.Ahead
	behind := status.Branch.Behind
	if ahead > 0 {
		res = append(res, FGGreen)
		res = append(res, "↑")
		res = append(res, strconv.Itoa(ahead))
	}
	if behind > 0 {
		res = append(res, FGRed)
		res = append(res, "↓")
		res = append(res, strconv.Itoa(behind))
	}

	// individual status code counts

	status_codes := [...]struct {
		code   StatusCode
		symbol string
		color  string
	}{
		{SCUnmodified /*         */, "", FGWhite},
		{SCUntracked /*         ?*/, "", FGYellow},
		{SCModified /*          M*/, "󱇨", FGMagenta},
		{SCAdded /*             A*/, "", FGGreen},
		{SCDeleted /*           D*/, "󰮉", FGRed},
		{SCRenamed /*           R*/, "", FGBlue},
		//{SCCopied /*            C*/, " ", FGWhite},
		{SCCopied /*            C*/, "", FGWhite},
		{SCUnmerged /*          U*/, "󰚰", FGWhite},
		{SCIgnored /*            */, "", FGWhite},
		{SCUnknown /*            */, "", FGWhite},
	}
	status_count := getGitStatusCodeCounts(status)
	for _, sc := range status_codes {
		if sc.symbol == "" {
			continue
		}
		count := status_count[sc.code]
		if count == 0 {
			continue
		}
		res = append(res, sc.color)
		res = append(res, sc.symbol)
		//res = append(res, FGBrightWhite)
		res = append(res, strconv.Itoa(count))
	}

	return NewPS1Part(res...), nil
}

// buildPromptPart returns a PS1Part instance for the prompt
func buildPromptPart() (*PS1Part, error) {
	username := os.Getenv("USER")
	lastexit := os.Getenv("LASTEXIT")

	var promptchar string
	if username == "root" {
		promptchar = "#"
	} else {
		promptchar = "\\$"
	}

	var promptcolor string
	if lastexit == "0" {
		promptcolor = FGBrightWhite
	} else {
		promptcolor = FGRedBold
	}

	return NewPS1Part(promptcolor, " ", promptchar, " "), nil
}

func main() {
	// Create a list of PS1PartBuilder functions
	builders := []struct {
		name        string
		fn          PS1PartBuilder
		alternative PS1PartBuilder
	}{
		{"PythonVenv", buildPythonVenvPart, nil},
		{"Time", buildTimePart, nil},
		{"Timewarrior", buildTimewarriorPart, nil},
		{"Username", buildUsernamePart, nil},
		{"Hostname", buildHostnamePart, nil},
		// {"Cwd", buildCwdPart, nil},
		{"Git", buildGitPart, buildCwdPart},
		{"Prompt", buildPromptPart, nil},
	}

	// Create a wait group to synchronize the builders
	var wg sync.WaitGroup

	// Create a map to receive the results from the builders
	// https://go.dev/blog/maps#concurrency
	var results = struct {
		sync.RWMutex
		m map[string]*PS1Part
	}{m: make(map[string]*PS1Part)}

	msgs := make([]*Message, 0)

	// Start each builder in a separate goroutine
	for _, builder := range builders {
		wg.Add(1)
		go func(builder struct {
			name        string
			fn          PS1PartBuilder
			alternative PS1PartBuilder
		}) {
			defer wg.Done()
			part, err := builder.fn()
			if err != nil {
				msgs = append(msgs, &Message{name: "Error", msg: fmt.Sprintf("while building part %s: %v\n", builder.name, err), iserr: true})
				return
			}
			results.Lock()
			results.m[builder.name] = part
			results.Unlock()
		}(builder)
	}

	// Create a channel to detect when all builders are done
	done := make(chan struct{})
	go func() {
		wg.Wait()
		close(done)
	}()

	// Wait for all builders to be done or the timeout to be reached
	select {
	case <-done: // all builders done
	case <-time.After(400 * time.Millisecond):
		msgs = append(msgs, &Message{name: "Timeout", msg: "some builders did not finish in time", iserr: true})
	}

	// Read the results into a list of parts in same order as builders
	parts := make([]*PS1Part, 0, len(builders))
	results.RLock()
	for _, builder := range builders {
		var err error
		part, ok := results.m[builder.name]
		if !ok {
			msgs = append(msgs, &Message{name: "Missing", msg: fmt.Sprintf("%s did not provide a part\n", builder.name), iserr: true})
			if builder.alternative != nil {
				part, err = builder.alternative()
				if err != nil {
					msgs = append(msgs, &Message{name: "Error", msg: fmt.Sprintf("failed building alternative part %s: %v\n", builder.name, err), iserr: true})
					continue
				}
			} else {
				continue
			}
		}
		for _, msg := range part.msgs {
			msgs = append(msgs, &Message{
				name:  builder.name,
				msg:   msg,
				iserr: false,
			})
		}
		parts = append(parts, part)
	}
	results.RUnlock()

	// Build the PS1 string from the parts list
	ps1 := RESET
	for _, part := range parts {
		ps1 += strings.Join(part.value, "")
	}
	ps1 += RESET

	for _, msg := range msgs {
		msg_split := strings.Split(msg.msg, "\n")
		for i, s := range msg_split {
			if i == len(msg_split)-1 && s == "" {
				continue
			}
			name := msg.name
			sep := ":"
			if i != 0 {
				name = ""
				sep = "|"
			}
			color := FGCyan
			if msg.iserr {
				color = FGRed
			}
			fullmsg := fmt.Sprintf("%s[bp] %s%*.*s%s%s %v%s", FGYellow, color, len(msg.name), len(msg.name), name, RESET, sep, s, RESET)
			fullmsg = strings.ReplaceAll(fullmsg, "\\[", "")
			fullmsg = strings.ReplaceAll(fullmsg, "\\]", "")
			fmt.Fprintln(os.Stderr, fullmsg)
		}
	}

	for _, part := range parts {
		for key, value := range part.vars {
			fmt.Printf("export %s='%s'\n", key, EscapeForBash(value))
		}
		for _, cmd := range part.cmds {
			fmt.Printf("%s\n", cmd)
		}
	}
	// Export the PS1 string
	fmt.Printf("export PS1='%s'\n", EscapeForBash(ps1))
}
