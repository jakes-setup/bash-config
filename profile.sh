# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022


## Fix SSH in kitty
#if [ "x${TERM}y" = "xxterm-kittyy" ]; then
#	export TERM=xterm-256color
#fi


#if [ "x${HOSTNAME}y" = "xRechenApparaty" ]; then
#	export TERMINAL=xfce4-terminal
#fi

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

add_dir_to_path() {
	if [ -d "$2" ] ; then
		echo "${2}:${1}"
	else
		echo "${1}"
	fi
}

export HD_UUID_Games="6b2c39c4-9ccf-42ef-ad78-bcabe8027590"
export HD_UUID_stuff="9e9f6af3-0f62-4908-9371-ce59e4e30d81"
export HD_UUID_Music="12089d06-0c51-4a66-b235-df949eaf5418"
export HD_UUID_Torrents="598cf437-6dc2-4028-a1a2-2feee836d13d"

if [ -e "/dev/disk/by-uuid/${HD_UUID_stuff}" ] && [ ! -d "/media/jake/stuff" ] ; then
	gio mount -d "${HD_UUID_stuff}"
fi
if [ -e "/dev/disk/by-uuid/${HD_UUID_Games}" ] && [ ! -d "/media/jake/Games" ] ; then
	gio mount -d "${HD_UUID_Games}"
fi
if [ -e "/dev/disk/by-uuid/${HD_UUID_Music}" ] && [ ! -d "/media/jake/Music" ] ; then
	gio mount -d "${HD_UUID_Music}"
fi
#if [ -e "/dev/disk/by-uuid/${HD_UUID_Torrents}" ] && [ ! -d "/media/jake/Torrents" ] ; then
#	gio mount -d "${HD_UUID_Torrents}"
#fi



PATH="$(add_dir_to_path "$PATH" "${HOME}/stuff/jakes-setup/neovim-config/neovim/build/bin")" # Jake neovim-config
PATH="$(add_dir_to_path "$PATH" "${HOME}/bin")"
PATH="$(add_dir_to_path "$PATH" "${HOME}/.bin")"
PATH="$(add_dir_to_path "$PATH" "${HOME}/.local/bin")"
PATH="$(add_dir_to_path "$PATH" "${HOME}/go/bin")"
PATH="$(add_dir_to_path "$PATH" "${HOME}/.gem/ruby/2.7.0/bin")"
PATH="$(add_dir_to_path "$PATH" "${HOME}/.cargo/bin")"
PATH="$(add_dir_to_path "$PATH" "${HOME}/.cabal/bin")"
PATH="$(add_dir_to_path "$PATH" "${HOME}/.ghcup/bin")"
PATH="$(add_dir_to_path "$PATH" "${HOME}/stuff/jakes-setup/bash-config/scripts")"
export PATH

LD_LIBRARY_PATH="$(add_dir_to_path "$LD_LIBRARY_PATH" "${HOME}/lib")"
LD_LIBRARY_PATH="$(add_dir_to_path "$LD_LIBRARY_PATH" "${HOME}/lib64")"
LD_LIBRARY_PATH="$(add_dir_to_path "$LD_LIBRARY_PATH" "${HOME}/.lib")"
LD_LIBRARY_PATH="$(add_dir_to_path "$LD_LIBRARY_PATH" "${HOME}/.lib64")"
export LD_LIBRARY_PATH



export VISUAL=vim
export EDITOR="$VISUAL"

# Set keyboard layout
## Set keyboard layout
#setxkbmap de
#setxkbmap -option caps:swapescape

export PASSWORD_STORE_ENABLE_EXTENSIONS=true

if hostname | grep -q '^c' ; then
	export GNUPGHOME=~/festplatte/current/stuff/j.vondoemming/gpg/
	# Maven
	if  [ -d "${ULSI_PREFIX}" ]; then
		export PATH=${PATH}:${ULSI_PREFIX}/opt/apache-maven-3.8.5/bin
	fi
fi
if [ -d "${ULSI_PREFIX}" ]; then
	# ulsi-install tesseract-ocr tesseract-ocr-deu tesseract-ocr-eng
	export TESSDATA_PREFIX="${ULSI_PREFIX}/usr/share/tesseract-ocr/5/tessdata/"
fi

# Brew
export HOMEBREW_NO_ANALYTICS=1
if [ -d "/home/linuxbrew/.linuxbrew" ]; then
	eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
fi

# see https://stackoverflow.com/questions/57591432/gpg-signing-failed-inappropriate-ioctl-for-device-on-macos-with-maven
GPG_TTY=$(tty)
export GPG_TTY

# RustUP
if [ -f "$HOME/.cargo/env" ]; then
	. "$HOME/.cargo/env"
fi

# GO
export GOWORK=off # disable go workspaces, as searching for go.work is really slow on CIP computers

# SSH Agent
if ! ( [ -n "${SSH_AUTH_SOCK}" ] && [ -S "${SSH_AUTH_SOCK}" ] && [ -n "${SSH_AGENT_PID}" ] && ps -p "${SSH_AGENT_PID}" > /dev/null ) ; then
	eval "$(ssh-agent)"
	ssh-add
#else
#	echo "SSH Agent is running as ${SSH_AGENT_PID}"
fi

# NIX
# See https://nixos.wiki/wiki/Nix_Installation_Guide#Installing_without_root_permissions
# and https://github.com/nix-community/nix-user-chroot
if [ "$(hostname --domain)" = "cip.loc" ]; then
	if [ -e /afs/informatik.uni-goettingen.de/user/j/j.vondoemming/.nix-profile/etc/profile.d/nix.sh ]; then
		# added by Nix installer
		. /afs/informatik.uni-goettingen.de/user/j/j.vondoemming/.nix-profile/etc/profile.d/nix.sh;
	else
		cat - <<EOF

Use
	nix-user-chroot ~/.nix bash -l
to switch into nix.
EOF
	fi
fi

true
export ULSI_PREFIX="/afs/informatik.uni-goettingen.de/user/j/j.vondoemming/.ulsi-prefix"; [ -d "${ULSI_PREFIX}" ] && . "${ULSI_PREFIX}/ulsi/env.sh" ; # ULSI-Load-Command
