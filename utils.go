package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
)

// Runs filepath.Abs and filepath.EvalSymlinks on the given path.
func AbsEvalSymlinks(path string) (string, error) {
	path, err := filepath.Abs(path)
	if err != nil {
		return "", err
	}
	return filepath.EvalSymlinks(path)
}


// FindFile searches for a file with any of the given filenames recursively in the parent directories.
func FindFile(filenames []string, startDir string) (string, error) {
	// Check if any of the file exist in the current directory.
	var fp string
	for _, filename := range filenames {
		fp = filepath.Join(startDir, filename)
		if _, err := os.Stat(fp); err == nil {
			return fp, nil // a file was found
		}
	}

	// If none of the files are found, move to the parent directory.
	parentDir := filepath.Dir(startDir)
	if parentDir == startDir {
		// We have reached the root directory, stop searching.
		return "", fmt.Errorf("No file found")
	}

	// Recursively search in the parent directory.
	return FindFile(filenames, parentDir)
}

// EscapeForBash safely escapes a string for usage in bash, enclosed in single quotes.
// WARNING: THIS FUNCTION IS NOT SAFE! EXAMPLE: Strings ending in backslashes can escape the escape!
// THIS FUNCTION IS mostly AI GENERATED!
func EscapeForBash(s string) string {
	var buf bytes.Buffer
	for _, r := range s {
		switch r {
		case '\033':
			buf.WriteString("\\e")
		case '\'':
			//buf.WriteString("'\\''")
			buf.WriteString("\\'")
		//case '\n', '\t', '\\':
		case '\n', '\t': // this is unsafe
			buf.WriteByte('\\')
			buf.WriteRune(r)
		default:
			buf.WriteRune(r)
		}
	}
	return buf.String()
}
