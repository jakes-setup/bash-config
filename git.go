package main

import (
	"fmt"
	"os/exec"
	"path/filepath"
	"slices"
	"strconv"

	"strings"
)

type StatusCode string

const (
	SCUnmodified StatusCode = "unmodified"
	SCUntracked  StatusCode = "untracked"
	SCModified   StatusCode = "modified"
	SCDeleted    StatusCode = "deleted"
	SCAdded      StatusCode = "added"
	SCRenamed    StatusCode = "renamed"
	SCCopied     StatusCode = "copied"
	SCUnmerged   StatusCode = "unmerged"
	SCIgnored    StatusCode = "ignored"
	SCUnknown    StatusCode = "unknown"
)

// BranchInfo represents the current branch information
type BranchInfo struct {
	OID      string
	Head     string
	Upstream string // upstream_branch or "" if upstream is not set
	Ahead    int    // number of commits ahead of upstream or 0 if upstream is not set
	Behind   int    // number of commits behind of upstream or 0 if upstream is not set
}

// ChangedEntry represents a changed tracked entry
type ChangedEntry struct {
	XY       string
	SC       StatusCode
	Sub      string
	MH       string
	MI       string
	MW       string
	HH       string
	HI       string
	Path     string
	OrigPath string
	Score    string
}

// UnmergedEntry represents an unmerged entry
type UnmergedEntry struct {
	XY   string
	SC   StatusCode
	Sub  string
	M1   string
	M2   string
	M3   string
	MW   string
	H1   string
	H2   string
	H3   string
	Path string
}

// UntrackedItem represents an untracked item
type UntrackedItem struct {
	Path string
}

// IgnoredItem represents an ignored item
type IgnoredItem struct {
	Path string
}

// GitStatus represents the parsed Git status output
type GitStatus struct {
	Branch      BranchInfo
	ChangedOrd  []ChangedEntry
	ChangedCopy []ChangedEntry
	Unmerged    []UnmergedEntry
	Untracked   []UntrackedItem
	Ignored     []IgnoredItem
}

func parseXY(XY string) StatusCode {
	// TODO this is way to specific for my usecase
	if XY == ".M" {
		return SCModified
	} else if strings.HasPrefix(XY, "D") {
		return SCDeleted
	} else if strings.HasSuffix(XY, "D") {
		return SCDeleted
	} else if strings.HasPrefix(XY, "A") {
		return SCAdded
	} else if strings.HasPrefix(XY, "R") {
		return SCRenamed
	} else {
		return SCUnknown
	}
}

func parseGitStatus(lines []string) (*GitStatus, error) {
	status := &GitStatus{}
	branch := &status.Branch

	for _, line := range lines {
		if line == "" {
			continue
		}

		if strings.HasPrefix(line, "#") {
			// Header line
			if strings.HasPrefix(line, "# branch.") {
				parts := strings.Split(line, " ")
				switch parts[1] {
				case "branch.oid":
					branch.OID = parts[2]
				case "branch.head":
					branch.Head = parts[2]
				case "branch.upstream":
					branch.Upstream = parts[2]
				case "branch.ab":
					branch.Ahead, _ = strconv.Atoi(parts[2][1:])
					branch.Behind, _ = strconv.Atoi(parts[3][1:])
				}
			}
			continue
		}

		// Tracked entry
		if strings.HasPrefix(line, "1") {
			parts := strings.Split(line, " ")
			//if len(parts) != 9 {
			//	return nil, fmt.Errorf("invalid changed entry line: %q", line)
			//}
			entry := ChangedEntry{
				XY:  parts[1],
				SC:  parseXY(parts[1]),
				Sub: parts[2],
				MH:  parts[3],
				MI:  parts[4],
				MW:  parts[5],
				HH:  parts[6],
				HI:  parts[7],
				//Path: parts[8], // TODO fix parsing
			}
			status.ChangedOrd = append(status.ChangedOrd, entry)
		} else if strings.HasPrefix(line, "2") {
			parts := strings.Split(line, " ")
			//fmt.Fprintf(os.Stderr, "parts: %v\n", parts)
			//fmt.Fprintf(os.Stderr, "len(parts): %v\n", len(parts))
			//if len(parts) != 11 {
			//	return nil, fmt.Errorf("invalid renamed/copied entry line: %q", line)
			//}
			entry := ChangedEntry{
				XY:    parts[1],
				SC:    parseXY(parts[1]),
				Sub:   parts[2],
				MH:    parts[3],
				MI:    parts[4],
				MW:    parts[5],
				HH:    parts[6],
				HI:    parts[7],
				Score: parts[8],
				//Path:     parts[9], // TODO fix parsing
				//OrigPath: parts[10],
			}
			status.ChangedCopy = append(status.ChangedCopy, entry)
		} else if strings.HasPrefix(line, "u") {
			parts := strings.Split(line, " ")
			//if len(parts) != 10 {
			//	return nil, fmt.Errorf("invalid unmerged entry line: %q", line)
			//}
			entry := UnmergedEntry{
				XY:  parts[1],
				SC:  parseXY(parts[1]),
				Sub: parts[2],
				M1:  parts[3],
				M2:  parts[4],
				M3:  parts[5],
				MW:  parts[6],
				H1:  parts[7],
				H2:  parts[8],
				H3:  parts[9],
				//Path: parts[10], // TODO fix parsing
			}
			status.Unmerged = append(status.Unmerged, entry)
		} else if line[0] == '?' {
			// Untracked item
			item := UntrackedItem{
				Path: line[2:],
			}
			status.Untracked = append(status.Untracked, item)
		} else if line[0] == '!' {
			// Ignored item
			item := IgnoredItem{
				Path: line[2:],
			}
			status.Ignored = append(status.Ignored, item)
		} else {
			return nil, fmt.Errorf("invalid line: %q", line)
		}
	}

	return status, nil
}

func getGitRootDir(cwd string) (string, error) {
	// Call `git rev-parse --show-toplevel` to get the root directory of the Git repository
	rootDirCmd := exec.Command("git", "rev-parse", "--show-toplevel")
	rootDirCmd.Dir = cwd
	rootDirOutput, err := rootDirCmd.CombinedOutput()
	if err != nil {
		return "", err
	}
	rootDir := strings.TrimSpace(string(rootDirOutput))
	res, err := filepath.Abs(rootDir)
	if err != nil {
		return "", err
	}
	return res, nil
}

func getGitRootDirs(cwd string) ([]string, error) {
	res := make([]string, 0)
	for {
		rootDir, err := getGitRootDir(cwd)
		if err != nil {
			break
		}
		res = append(res, rootDir)
		cwd = filepath.Dir(rootDir)
		if cwd == "/" {
			break
		}
	}
	slices.Reverse(res)
	return res, nil
}

func getGitStatus(cwd string) (*GitStatus, error) {
	// Call `git status --porcelain=2 --branch` to get the status of the Git repository
	statusCmd := exec.Command("git", "status", "--porcelain=2", "--branch")
	statusCmd.Dir = cwd
	statusOutput, err := statusCmd.CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("failed to get status: %v", err)
	}

	// Parse the output of `git status --porcelain=2 --branch`
	statusLines := strings.Split(string(statusOutput), "\n")
	status, err := parseGitStatus(statusLines)
	if err != nil {
		return nil, fmt.Errorf("failed to parse status: %v", err)
	}
	return status, nil
}

func getGitStatusCodeCounts(status *GitStatus) map[StatusCode]int {
	res := make(map[StatusCode]int)
	for _, entry := range status.ChangedOrd {
		_, ok := res[entry.SC]
		if !ok {
			res[entry.SC] = 0
		}
		res[entry.SC] += 1
	}
	for _, entry := range status.ChangedCopy {
		_, ok := res[entry.SC]
		if !ok {
			res[entry.SC] = 0
		}
		res[entry.SC] += 1
	}
	// res[SCModified] = len(status.ChangedOrd) // TODO ähm warum ist das auch über parseXY?
	res[SCCopied] = len(status.ChangedCopy)
	res[SCUnmerged] = len(status.Unmerged)
	res[SCUntracked] = len(status.Untracked)
	res[SCIgnored] = len(status.Ignored)
	return res

}
