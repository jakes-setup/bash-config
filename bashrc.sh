# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
	*i*) ;;
	*) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
fi

## This Changes The PS1
export PROMPT_COMMAND=(
	"export LASTEXIT=\"\${PIPESTATUS[-1]}\""
	"eval \" \$(bashprompt)\" || export PS1=\"PS1 Error! \W -> \""
	"export _run_after_prompt=\"yes\""
)

export PREVPROMPTTS="$EPOCHSECONDS"
export _run_after_prompt=no
# https://unix.stackexchange.com/questions/688315/run-command-after-prompt
trap '[[ "$_run_after_prompt" == "yes" ]] && export PREVPROMPTTS="$EPOCHSECONDS" || true; _run_after_prompt=no' DEBUG

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

FORTUNE="fortune -s"

for COWNAME in `cowsay -l | tail -n +2`
do
COWS+=$COWNAME
COWS+='\n'
done
COWS=${COWS%??}
RANDOMCOW=$(echo -e $COWS | sort -R | head -n 1)
$FORTUNE | cowsay -f $RANDOMCOW | lolcat

set -o vi

#eval "$(thefuck --alias)"

#tmux

#df -H

# Start screenfetch
#screenfetch -w
# Start neofetch
#neofetch
# Start fastfetch
~/stuff/jakes-setup/bash-config/fastfetch --logo kiss

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

[ -f "${HOME}/.ghcup/env" ] && source "${HOME}/.ghcup/env" # ghcup-env


#if [ -f /etc/gentoo-release ] || ( [ -f /etc/os-release ] && grep -q -i "manjaro" "/etc/os-release"  ); then
#	python -m venv "${HOME}/venv"
#	. "${HOME}/venv/bin/activate"
#fi


# Let Ctrl+p in Bash automatically start vim with Ctrl+p
function __nvim_ctrlp() {
	local GITDIR="$(git rev-parse --show-toplevel 2>/dev/null)"
	if [ -n "${GITDIR}" ]; then
		nvim -c 'execute "normal \<C-p>"'
	else
		echo "Ctrl+p is only available in git repositories."
	fi
}
export -f __nvim_ctrlp
PREVCTRLPBIND="$(bind -P | grep '"\\C-p"' | cut -d ' ' -f 1 | head -n 1)"
if [ -n "${PREVCTRLPBIND}" ]; then
	#echo "Unbinding '${PREVCTRLPBIND}'"
	bind -u "${PREVCTRLPBIND}"
fi
bind -x '"\C-p": __nvim_ctrlp'

true
