#!/bin/bash

set -e

#PARENT_COMMAND="$(ps -o args= $PPID)"
#if [ "$PARENT_COMMAND" = "sh -c ~/.vim/makehelper.sh ; true" ]; then
#	FROMVIM="t"
#else
#	FROMVIM="f"
#fi

tput sgr0
#printf '\r%*s' "$(tput cols)" "" # Clear line

#if [ "$FROMVIM" = "t" ]; then
#	clear
#fi

if [ -f './Makefile' ]; then
   make "$@"
elif [ -f '../Makefile' ]; then
   make -C .. "$@"
elif [ -f '../../Makefile' ]; then
   make -C ../.. "$@"
elif [ -f '../../../Makefile' ]; then
   make -C ../../.. "$@"
elif [ -f '../../../../Makefile' ]; then
   make -C ../../../.. "$@"
elif [ -f '../../../../../Makefile' ]; then
   make -C ../../../../.. "$@"
elif [ -f '../../../../../../Makefile' ]; then
   make -C ../../../../../.. "$@"
else
	GITDIR="$(git rev-parse --show-toplevel 2>/dev/null || true)"
	export GITDIR
	if [ -n "${GITDIR}" ] && [ -f "${GITDIR}/build.gradle" ] && command -v gradle &> /dev/null ; then
		gradle --project-dir "${GITDIR}" "$@"
	else
		ant -find "$@"
	fi
fi

#if [ "$FROMVIM" = "t" ]; then
#	read -n 1 YEET
#fi
true

