package main

//const ESC string = "\\e["
const ESC string = "\\[\033["
const ESCEND string = "m\\]"
const RESET string = ESC + "0" + ESCEND
const (
	FGBlack = ESC + "0;" + "3" + string('0'+iota) + ESCEND
	FGRed
	FGGreen
	FGYellow
	FGBlue
	FGMagenta
	FGCyan
	FGWhite
)
const (
	FGBrightBlack = ESC + "0;" + "9" + string('0'+iota) + ESCEND
	FGBrightRed
	FGBrightGreen
	FGBrightYellow
	FGBrightBlue
	FGBrightMagenta
	FGBrightCyan
	FGBrightWhite
	FGGray = ESC + "0;" + "90" + ESCEND
)
const (
	BGBlack = ESC + "0;" + "4" + string('0'+iota) + ESCEND
	BGRed
	BGGreen
	BGYellow
	BGBlue
	BGMagenta
	BGCyan
	BGWhite
)
const (
	BGBrightBlack = ESC + "0;" + "10" + string('0'+iota) + ESCEND
	BGBrightRed
	BGBrightGreen
	BGBrightYellow
	BGBrightBlue
	BGBrightMagenta
	BGBrightCyan
	BGBrightWhite
	BGGray = ESC + "0;" + "100" + ESCEND
)

const (
	FGBlackBold = ESC + "0;1;" + "3" + string('0'+iota) + ESCEND
	FGRedBold
	FGGreenBold
	FGYellowBold
	FGBlueBold
	FGMagentaBold
	FGCyanBold
	FGWhiteBold
)
const (
	FGBrightBlackBold = ESC + "0;1;" + "9" + string('0'+iota) + ESCEND
	FGBrightRedBold
	FGBrightGreenBold
	FGBrightYellowBold
	FGBrightBlueBold
	FGBrightMagentaBold
	FGBrightCyanBold
	FGBrightWhiteBold
	FGGrayBold = ESC + "0;1;" + "90" + ESCEND
)
const (
	BGBlackBold = ESC + "0;1;" + "4" + string('0'+iota) + ESCEND
	BGRedBold
	BGGreenBold
	BGYellowBold
	BGBlueBold
	BGMagentaBold
	BGCyanBold
	BGWhiteBold
)
const (
	BGBrightBlackBold = ESC + "0;1;" + "10" + string('0'+iota) + ESCEND
	BGBrightRedBold
	BGBrightGreenBold
	BGBrightYellowBold
	BGBrightBlueBold
	BGBrightMagentaBold
	BGBrightCyanBold
	BGBrightWhiteBold
	BGGrayBold = ESC + "0;1;" + "100" + ESCEND
)
