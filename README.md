# Bash Config

## Abhängigkeiten

- Bash
- Go
- wget
- UnZip
- make
- git
- fortune
- cowsay
- time

## Installieren

```bash
git clone --recurse-submodules git@gitlab.gwdg.de:jakes-setup/bash-config.git ~/stuff/jakes-setup/bash-config
~/stuff/jakes-setup/bash-config/update.sh
```

## Automatische timewarrior Erinnerungen

Falls im aktuellen Ordner oder irgendwo dadrüber eine `timewarrior.json` liegt werden automatische Erinnerungen ausgegeben, dass man seine Zeit tracken sollte.

Schema:
```json
{
    "task": "<name of the task>",
    "cancel": <optional boolean to tell the prompt to stop the search here>
}
```
